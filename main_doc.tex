
\documentclass{article}
\usepackage{graphicx}
\usepackage{url}
\usepackage{verbatim}
\title{Mixeos : Custom EOS Module for MESA}
\author{Neil Miller}
\begin{document}
\maketitle
\section{Objective}
The mixeos module aims to allow the user to mix an arbitrary species
using a rule such as the additive volume rule for an input mass fraction
vector.  The additive volume method assumes that species are not interacting
and simply take up space

\begin{equation}
\frac{1}{\rho(P,T)} = \sum \frac{X_i}{\rho(P,T)}
\end{equation}

This is easy to implement as the additive volume is linear in
specific volume, entropy and internal energy.

\begin{eqnarray}
  v(P,T) &=& \sum X_i v_i(P,T) \\
  S(P,T) &=& \sum X_i S_i(P,T) \\
  E(P,T) &=& \sum X_i E_i(P,T)
\end{eqnarray}

It is a general aim of this code to be easily modifyable such that
users can - with minimal effort - add their own equation of state
to see how this affects the system being modeled.  An equation of
state could potentially be implemented as an analytic expression
such as the ideal gas law.  It could also be represented through
a tabular method.  One tabular method that we favor is described
by Timmes and Swesty where a polynomial surface is determined
for the free energy.  The Timmes method produces free energy derivatives
which allow for continuous a thermodynamic self-consistent eos at each point.

The user may also have a tabular representation for Pressure, Internal Energy,
and Entropy as independent tables.  A disadvantage of this method
is that the tables may not be self consistent with each other.

\section{Additive Volume Formulas and Derivation}

\section{Design Overview}
The mixeos library is actually composed of multiple fortran modules
in the MESA style.  There are public modules which are meant to 
be accessed by routines outside of the mixeos library.  These
provide the standard interface that is required by MESA for an 
alternative EOS.  

There are also private modules.  The private modules focus on the
actual details of adding N equations of state using the additive
volume method.  In order to allow for the easiest extensibility,
an individual eos is a derived class of the base\_eos object.
The base\_eos describes a standard interface that an implementation
must conform to.  When the mixeos module is handling the implementations
it does not need to, nor is it desireable, for it to know the details
of that implementation.  A disadvantage may be that the object oriented
fortran features could potentially be slower than a direct implementation,
or potentially even buggy as they are relatively new.  
The private module mixoes\_mod puts everything together by implementing
the required interface functions.  It uses tested thermodynamic
subroutines for standard transformations.  The eos tables are stored
in a list - as pointers to the parent class.  This is where the polymorphic
behavior is occuring since mixeos\_mod doesn't know how the eos for
each material is implemented.

\section{Design Details}

\section{MESA standard Hydrogen-Helium EOS}
MESA provides a Hydrogen-Helium eos that is designed to work over many orders of magnitude.
The library provides function calls that work in Density-Temperature space as well as 
Pressure-Temperature space.  MESA has been designed so that the dependent coordinates
can be either Density-Temperature or Pressure-Temperature.  To provide, this MESA constructs
a pre-computed tabular representation for both coordinate systems.

Because, we are using the additive volume rule, it is faster to call the mixeos library with
pressure temperature.  


\section{Testing the library}
%% Include plots that show the system is working
%% Describe the tests that have been done

To check that the additive volume rule is working correctly, the 
density, internal energy and entropy have been plotted as a function
of water mass fraction for a fixed Pressure and Temperature.  The
analytic relationship is plotted in red and perfectly fits these points.
I use the end points of Z water = 0 and Z water = 1 to calculate the analytic
relationship.  This shows that the additive volume rule is self consistently 
being implemented.  


\begin{figure}[h]

\includegraphics[width=10cm]{TestMixeosFigs/Z_PT_logRho}
\caption{The density as a function of water mass fraction for fixed pressure = $10^{13}$ 
   and temperature = $10^5$.  The points are the output density from calls to the mixeos
   library.  The red line is the analytic relationship that is determined by using the 
   endpoints.  This shows no unexpected behavior in the additive volume rule.}
\label{testAddVol_logRho}
\end{figure}

\begin{figure}[h]
\includegraphics[width=10cm]{TestMixeosFigs/Z_PT_logE}
\caption{The internal energy as a function of water mass fraction for fixed pressure = $10^{13}$
   and temperature = $10^5$.  The red line is the analytic relationship that is derived using
   the endpoints.  This plot shows no unexpected behavior from the additive volume rule.}
\label{testAddVol_logE}
\end{figure}

\begin{figure}[h]
\includegraphics[width=10cm]{TestMixeosFigs/Z_PT_logS}
\caption{The entropy as a function of water mass fraction for fixed pressure = $10^{13}$ and 
   temperature = $10^5$.  The red line is the analytic relationship that is derived using the endpoints.
   This plot shows no unexpected behavior from the additive volume rule.}
\end{figure}

\section{Using the EOS in MESA}
%% 
The mixeos code repository is on bitbucket at \url{https://bitbucket/org/NeilMiller/mixeos}.
After cloning the repository to a local directory, you will need to build and export the 
library to your MESA library.  

If you are planning on using your own equation of state tables, then you may want to construct
them at this point.  This task may not be trivial.  The mixeos currently implements the helmholtz
free energy equation of state table, the ideal gas analytic equation of state (for testing),
and I am working to include another representation, which I call the three-table approach.  
The $\log(P)$, $\log(E)$, $\log(S)$ tables are independently passed to this equation of state.
The downside of this approach is that the output is not guarenteed to be thermodynamically
self consistent.  However, it may be easier to implement than the helmholtz free energy 
approach.  Just to get started, you may want to use the ideal gas implementation and then
do this step later.

\subsection{Setting up the run}


Append water to the list of isotopes (chem/chem\_data/isotopes.data)
\begin{verbatim}
water      18.0     0   18   0.0   0.0
 1.0E+0  1.0E+0  1.0E+0  1.0E+0  1.0E+0  1.0E+0  1.0E+0  1.0E+0
 1.0E+0  1.0E+0  1.0E+0  1.0E+0  1.0E+0  1.0E+0  1.0E+0  1.0E+0
 1.0E+0  1.0E+0  1.0E+0  1.0E+0  1.0E+0  1.0E+0  1.0E+0  1.0E+0
\end{verbatim}
The user should add any other species in a similar way that are defined in the mixeos library.

The nuclear network needs to also be modified.  Here is an example file of my ``planet.net''
\begin{verbatim}
      add_isos(
         h1 
         h2
         he3 
         he4 
         c12 
         o16
         water
         )
         
      add_reactions(
         ! pp chains
         rpp_to_he3               ! p(p e+nu)h2(p g)he3
         rpep_to_he3              ! p(e-p nu)h2(p g)he3     
         r_he3_he3_to_h1_h1_he4   ! he3(he3 2p)he4 
         r34_pp2                  ! he4(he3 g)be7(e- nu)li7(p a)he4 
         r34_pp3                  ! he4(he3 g)be7(p g)b8(e+ nu)be8( a)he4  
         r_h1_he3_wk_he4          ! he3(p e+nu)he4     
         ! auxiliaries -- used only for computing rates of other reactions
         rbe7ec_li7_aux 
         rbe7pg_b8_aux 
         )     
\end{verbatim}     
     
     
     

Inside the run/src, Modify the file run\_star\_extras.f so that the ``extra\_controls'' routine 
tell MESA to use our custom ``other\_eos''.
\begin{verbatim}
      subroutine extras_controls(s, ierr)
         type (star_info), pointer :: s
         integer, intent(out) :: ierr
         real(dp) :: mu
         character (len=256) :: name
         
         ierr = 0

         write(*,*) "Setting custome eos => mixeos"
         s% other_eosDT_get              => mixeos_get
         s% other_eosDT_get_T            => mixeos_get_t
         s% other_eosDT_get_Rho          => mixeos_get_rho
         s% other_eosPT_get              => mixeosPT_get
         s% other_eosPT_get_T            => mixeosPT_get_T
         s% other_eosPT_get_Pgas         => mixeosPT_get_Pgas
         s% other_eosPT_get_Pgas_for_Rho => mixeosPT_get_Pgas_for_Rho
         s% use_other_eos = .true.
         
         name = "water"
         mu = 18.d0
         call mixeos_add_ideal(mu, name)
      end subroutine extras_controls
      
\end{verbatim}

This connects MESA to our custom other\_eos library.  The line $\textrm{s\% use\_other\_eos = .true.}$ is the 
switch that MESA uses to determine if it should use these other\_eos functions.

The mixeos library has been setup such that the user calls initialization functions like
mixeos\_add\_ideal.  This function both creates an object for an ideal gas equation of state
with $\mu = 18.$ as well as adds it to the list of metal EOS tables inside of mixeos.  
Because the implementation is object-oriented, other eos representations built in a similar
way and integrated into the mixeos library.  
\section{Adding a new implementation}
%% Walk through the required implementation of a different representation
%%  (Perhaps implement ANEOS direct)

\end{document}
